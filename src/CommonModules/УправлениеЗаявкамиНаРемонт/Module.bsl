//**************************************************//
//													//
//													//
//**************************************************//

#Область ЗаявкиНаРемонт

Процедура УстановитьСтатусЗаявкиНаРемонт(ЗаявкаНаРемонт, ДополнительныеПараметры) Экспорт 
	
	ТекущийСтатусЗаявки = РегистрыСведений.СтатусыЗаявокНаРемонтОборудования.ПолучитьСтатусЗаявкиНаРемонтОборудования(ЗаявкаНаРемонт);
	Статус = СформироватьСтатусЗаявкиНаРемонт(ЗаявкаНаРемонт, ДополнительныеПараметры);
	РегистрыСведений.СтатусыЗаявокНаРемонтОборудования.УстановитьСтатусЗаявкиНаРемонтОборудования(ЗаявкаНаРемонт, Статус);
	
КонецПроцедуры

Функция СформироватьСтатусЗаявкиНаРемонт(ЗаявкаНаРемонт, ДополнительныеПараметры)
	
	КоличествоПоЗаявке = 0;
	КоличествоПоОтгрузке = 0;
	КоличествоПриход = 0;
	ЕстьРасход = Ложь;
	ЕстьПриход = Ложь;
	
	МассивРезультатов = ТабличнаяЧастьДокументаСервер.ПодготовитьДанныеОтгруженныхОприходованныхТоваровПоЗаявке(ЗаявкаНаРемонт, ДополнительныеПараметры);
	
	///// **** результат запроса по заявке 
	РезультатПоЗаявке = МассивРезультатов[0].Выбрать();
	Пока РезультатПоЗаявке.Следующий() Цикл 
		КоличествоПоЗаявке = РезультатПоЗаявке.Количество;
	КонецЦикла;
	/////// **** результат запроса по Расходным Ордерам
	РезультатПоРасходу = МассивРезультатов[1].Выбрать();
	Пока РезультатПоРасходу.Следующий() Цикл 
		КоличествоПоОтгрузке = РезультатПоРасходу.Количество;
		Если КоличествоПоОтгрузке <> 0 Тогда 
			ЕстьРасход = Истина;
		КонецЕсли;
	КонецЦикла;
	Если НЕ ЕстьРасход Тогда
		Возврат  Перечисления.СтатусыЗаявкиНаРемонт.ПодготовленКРемонту;   /// возврат при отсутствии отгрузок
	КонецЕсли;
	////// **** результат запроса по Приходным Ордерам
	РезультатПоПриходу = МассивРезультатов[2].Выбрать();
	Пока РезультатПоПриходу.Следующий() Цикл
		КоличествоПриход = РезультатПоПриходу.Количество;
		Если КоличествоПриход <> 0 Тогда 
			ЕстьПриход = Истина;
		КонецЕсли;
	КонецЦикла;
	/// *****статус: @Полностью выполнена заявка 
	Если ЕстьРасход И ЕстьПриход Тогда
		РасходПриход = КоличествоПоОтгрузке - КоличествоПриход;
		Если КоличествоПоЗаявке = КоличествоПоОтгрузке Тогда
			Если РасходПриход = 0 Тогда 
				Возврат Перечисления.СтатусыЗаявкиНаРемонт.ОприходованПолностью;
			Иначе
				Возврат Перечисления.СтатусыЗаявкиНаРемонт.ОжидаетВозвратЧастично;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	/// ***** статус: @Передан В Ремонт, статус: @ПереданВРемонтЧастично
	Если ЕстьРасход И НЕ ЕстьПриход Тогда 
		Если КоличествоПоЗаявке = КоличествоПоОтгрузке Тогда
			Возврат Перечисления.СтатусыЗаявкиНаРемонт.ПереданВРемонт;
		КонецЕсли;
		Если КоличествоПоОтгрузке < КоличествоПоЗаявке Тогда
			Возврат Перечисления.СтатусыЗаявкиНаРемонт.ПереданВРемонтЧастично;
		КонецЕсли;
	КонецЕсли;
	/// **** статус: @Ожидает возврат, статус: @Частично оприходован / Ожидает возврат
	Если ЕстьРасход И ЕстьПриход Тогда 
		Если КоличествоПоЗаявке = КоличествоПоОтгрузке И КоличествоПриход = 0 Тогда 
			Возврат Перечисления.СтатусыЗаявкиНаРемонт.ОжидаетВозврат;
		КонецЕсли;
		Если КоличествоПоЗаявке >= КоличествоПоОтгрузке И КоличествоПриход <> 0 Тогда 
			Возврат Перечисления.СтатусыЗаявкиНаРемонт.ОжидаетВозвратЧастично;
		КонецЕсли;
	КонецЕсли;
	
	Возврат  Перечисления.СтатусыЗаявкиНаРемонт.ПодготовленКРемонту; /// *** Дефолтный возврат)
	
	
КонецФункции

#КонецОбласти