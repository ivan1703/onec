#Region HTTP_ACTION

Function SendRequestForProcessing(StructuredData) Export
		
	recordJSON = New JSONWriter;
	recordJSON.SetString();
	XDTOSerializer.WriteJSON(recordJSON, StructuredData.sendstring, XMLTypeAssignment.Explicit);
	RequestString = recordJSON.Close(); 
	
	StructuredConnection = GetAuthorizationData(StructuredData.ReceptionBase, StructuredData.TypeData);
	
	connection = New HTTPConnection(StructuredConnection.Server,,StructuredConnection.Login, StructuredConnection.Pass);
	
	request = New HTTPRequest();
	
	request.ResourceAddress = StructuredConnection.ResourceAddress;
	request.Headers.Insert("Content-type", "application/json; charset=utf-8");
	request.Headers.Insert("User-Agent", "1C+Enterprise/8.3");
	request.SetBodyFromString(RequestString, TextEncoding.UTF8, ByteOrderMarkUse.DontUse);
	
	result = connection.Post(request);
	
	return result;   
			
EndFunction

#EndRegion

#Region UtilityFunctions

Function GetAuthorizationData(ReceptionBase, TypeData)
	
	StructuredData = New Structure("Server, ResourceAddress, Login, Pass");
		
	Query = Новый Query;
	Query.Text = 
		"ВЫБРАТЬ
		|	ДанныеДляОбмена.ReceptionBase КАК ReceptionBase,
		|	ДанныеДляОбмена.Login КАК Login,
		|	ДанныеДляОбмена.Pass КАК Pass,
		|	ДанныеДляОбмена.Server КАК Server,
		|	ДанныеДляОбмена.ResourceAddress КАК ResourceAddress
		|ИЗ
		|	РегистрСведений.ДанныеДляОбмена КАК ДанныеДляОбмена
		|ГДЕ
		|	ДанныеДляОбмена.ReceptionBase = &ReceptionBase
		|	И ДанныеДляОбмена.TypeData = &TypeData";
	
	Query.SetParameter("ReceptionBase", ReceptionBase);
	Query.SetParameter("TypeData", TypeData);
	Result = Query.Execute().Select();
	
	if Result.Количество() = 0 then
		return false;
	endif;
	
	While Result.Next() Do
		FillPropertyValues(StructuredData, Result);
	EndDo;
	
	return StructuredData;
	
EndFunction

#EndRegion