
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Ключ.Ссылка.Пустая() Тогда
		
		РаботаСПользователями.ЗаполнитьРеквизитыПоУмолчанию(Объект);
		Объект.Статус = Перечисления.СтатусыЗаявокНаОтгрузку.ВОбработке;
		Объект.Дата = ТекущаяДата();
		
	КонецЕсли;
	
	УстановитьПараметрыОтбораНоменклатуры();

КонецПроцедуры

&НаКлиенте
Процедура УстановитьСклад(Команда)
	// Вставить содержимое обработчика.
КонецПроцедуры

&НаКлиенте
Процедура ТипЗаявкиПриИзменении(Элемент)
	УстановитьПараметрыОтбораНоменклатуры();
КонецПроцедуры

&НаСервере
Процедура УстановитьПараметрыОтбораНоменклатуры()
	
	Если ЗаявкаНаПостамат() Тогда 
		Выбор = Новый Массив;
		Выбор.Добавить(Новый ПараметрВыбора("Отбор.ВидНоменклатуры", Справочники.ВидыНоменклатуры.Постаматы));	
	Иначе
		Выбор = Новый Массив;
	КонецЕсли;
	
	Элементы.ТоварыКОтгрузкеНоменклатура.ПараметрыВыбора = Новый ФиксированныйМассив(Выбор);
		
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Если ЗаявкаНаПостамат() Тогда 
		КоличествоСтрок = ТекущийОбъект.ТоварыКОтгрузке.Количество();
		Если КоличествоСтрок <> 1 Тогда 
			Отказ = Истина;
			Сообщение = СтрШаблон("Тип заявки: %1, При даннома типе в табличной части допускается одна запись", Объект.ТипЗаявки);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Сообщение);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ЗаявкаНаПостамат()
	
	Если Объект.ТипЗаявки = Перечисления.ТипыЗаявокНаОтгрузку.ЗаявкаНаПостамат Тогда
		Возврат Истина;
	КонецЕсли;	
	
	Возврат Ложь;
		
КонецФункции

&НаСервере
Процедура УстановитьСтатусОбъекта()
	
	Объект.Статус = РегистрыСведений.СтатусыЗаявокНаОтгрузку.ПолучитьСтатусЗаявкиНаОтгрузку(Объект.Ссылка);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	УстановитьСтатусОбъекта();
	
КонецПроцедуры
