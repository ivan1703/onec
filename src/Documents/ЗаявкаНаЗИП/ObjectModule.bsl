#Область ОбработчикиСобытий

Перем Перепроведение;

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	Если Перепроведение Тогда
		Возврат;
	КонецЕсли;
	
	Ошибки = Неопределено;
	
	МодульПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения = Неопределено);
	ЗаявкиНаОтгрузкуСервер.ИнициализироватьДанныеЗаявкаЗИП(ДополнительныеСвойства);
	ЗаявкиНаОтгрузкуСервер.ИнициализироватьДанныеЗаявкаЗИПВозврат(ДополнительныеСвойства);
	МодульДвижениеПоРегистрам.СформироватьДвижениеПоТоварыВРезерв(ДополнительныеСвойства, Движения, Отказ);
	МодульДвижениеПоРегистрам.СформироватьДвижениеПоТоварамВЗаявках(ДополнительныеСвойства, Движения, Отказ);
	МодульДвижениеПоРегистрам.СформироватьДвижениеПоТоварыНаВозврат(ДополнительныеСвойства, Движения, Отказ);
	
	ТаблицаТоварыНаСкладах = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаТоварыНаСкладах;
	ОбщегоНазначенияКонтрольОстатков.ПроверитьСоответствиеОстаткамРегистра(ТаблицаТоварыНаСкладах ,Ошибки, Отказ, Истина);
	
	Если Отказ И Ошибки <> Неопределено Тогда 
		Для Каждого Стр Из Ошибки Цикл
			СтрокаОтвет = СтрШаблон("Позиция: %1 нет свободных остатков в количестве: %2. на Складе - %3", Стр.Номенклатура, Стр.Количество,Стр.Склад); 
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(СтрокаОтвет); 
		КонецЦикла;
		Возврат
	КонецЕсли;
		
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если НЕ Перепроведение Тогда  
		РегистрыСведений.СтатусыЗаявокНаОтгрузку.УстановитьСтатусЗаявкиНаОтгрузку(Ссылка, СтатусЗаявки);
	КонецЕсли;

КонецПроцедуры

Процедура ПередЗаписью(Отказ)
	
	Перепроведение = ЭтотОбъект.Проведен;
	
КонецПроцедуры


#КонецОбласти
