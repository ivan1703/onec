
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ТаблицаМатериалов = ЗначениеИзСтрокиВнутр(Параметры.Материалы);
	
	ДобавляемыеРеквизиты = Новый Массив;
	Реквизит_Материалы = Новый РеквизитФормы("Материалы", Новый ОписаниеТипов("ТаблицаЗначений"),,"Материалы");
	Реквизит_Номенклатура = Новый РеквизитФормы("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"), "Материалы", "Номенклатура");
	ДобавляемыеРеквизиты.Добавить(Реквизит_Материалы);
	ДобавляемыеРеквизиты.Добавить(Реквизит_Номенклатура);
	ЭтаФорма.ИзменитьРеквизиты(ДобавляемыеРеквизиты);
		
	////////////
	
	ГруппаФормы = Элементы.ГруппаМатериалы;
	ЭлементФормы = Элементы.Добавить("Материалы", Тип("ТаблицаФормы"), ГруппаФормы);
    ЭлементФормы.ПутьКДанным = "Материалы";
	
	НоваяКолонка = Элементы.Добавить("МатериалНоменклатура", Тип("ПолеФормы"), Элементы.Материалы); 
	НоваяКолонка.Заголовок = "Номенклатура"; 
	НоваяКолонка.ПутьКДанным = "Материалы.Номенклатура";
	НоваяКолонка.Вид = ВидПоляФормы.ПолеВвода; 
	НоваяКолонка.РежимРедактирования = РежимРедактированияКолонки.ВходПриВводе; 
	
	////////////
	
	
КонецПроцедуры


