
&НаСервере
Процедура ТоварыКОтгрузкеСкладПриИзмененииНаСервере()
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКОтгрузкеСкладПриИзменении(Элемент)
	ТоварыКОтгрузкеСкладПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Ключ.Ссылка.Пустая() Тогда 
		
		РаботаСПользователями.ЗаполнитьРеквизитыПоУмолчанию(Объект);
		Объект.Дата = ТекущаяДатаСеанса();
		
	КонецЕсли;
	
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма)


КонецПроцедуры

&НаКлиенте 
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект)
	
КонецПроцедуры 

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПроведенПриОткрытии(ТекущийОбъект)
	
КонецПроцедуры

&НаСервере
Процедура ПроведенПриОткрытии(ТекущийОбъект)
	
	УстановитьДоступностьЭлементовФормы(ТекущийОбъект);
	
	Если ТекущийОбъект.Проведен Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Документ проведен, редактирование запрещено! если Вы хотите изменить документ? отмените проведение");
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьЭлементовФормы(ТекущийОбъект)
	
	Элементы.ГруппаВерх.Доступность = НЕ ТекущийОбъект.Проведен;
	Элементы.Страницы.ТолькоПросмотр = ТекущийОбъект.Проведен;
	Элементы.ГруппаДата.Доступность = НЕ ТекущийОбъект.Проведен;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	УстановитьДоступностьЭлементовФормы(ТекущийОбъект);
КонецПроцедуры
	
