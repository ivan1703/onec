#Область ЗаписиРегистра

Процедура ИзменитьЗаписьРегистра(ДанныеЗаписи) Экспорт
	
   ЗаписьРегистра = РегистрыСведений.СобытияУведомления.СоздатьМенеджерЗаписи();
   ЗаполнитьЗначенияСвойств(ЗаписьРегистра, ДанныеЗаписи);
   ЗаписьРегистра.Прочитан = Истина;
   
   	Попытка
		ЗаписьРегистра.Записать();
	Исключение
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ОписаниеОшибки());
	КонецПопытки;
	
КонецПроцедуры

// Процедура - Создать новую запись
//
// Параметры:  Струутура
//  ДанныеЗаписи - { Получатель (Пользователи), Объект (ДокументСсылка),Прочитан (Буль) } 
//
Процедура СоздатьНовуюЗапись(ДанныеЗаписи) Экспорт
	
	Если НЕ ТипЗнч(ДанныеЗаписи) = Тип("Структура") Тогда 
		Возврат;
	КонецЕсли;
	
	МенеджерЗаписи = РегистрыСведений.СобытияУведомления.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, ДанныеЗаписи);
	
	Попытка
		МенеджерЗаписи.Записать();
	Исключение
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ОписаниеОшибки());
	КонецПопытки;

КонецПроцедуры

#КонецОбласти