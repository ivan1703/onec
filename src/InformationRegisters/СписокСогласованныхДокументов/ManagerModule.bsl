//*****************************************************************//
//                                                                 //
//                                                                 //
//*****************************************************************//

#Область ОбработкаСтатусаСогласование

Функция ТекущееСогласованиеДокумента(Документ) Экспорт
		
	Запрос = Новый Запрос("ВЫБРАТЬ СписокСогласованныхДокументов.Согласован КАК Согласован
		|ИЗ РегистрСведений.СписокСогласованныхДокументов КАК СписокСогласованныхДокументов
		|ГДЕ СписокСогласованныхДокументов.Документ = &Документ");
	
	Запрос.УстановитьПараметр("Документ", Документ);
	Результат = Запрос.Выполнить().Выбрать();
	
	Если Результат.Количество() = 0 Тогда 
		Возврат Перечисления.СтатусСогласованияДокумента.ПустаяСсылка();
	КонецЕсли;
	
	Пока Результат.Следующий() Цикл
		Возврат Результат.Согласован;
	КонецЦикла;
	
КонецФункции

Процедура СогласоватьДокумент(Документ, Статус) Экспорт
	
	МенеджерЗаписи = РегистрыСведений.СписокСогласованныхДокументов.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Документ = Документ;
	МенеджерЗаписи.Согласован = Статус;
	
	МенеджерЗаписи.Записать();
	
КонецПроцедуры

Функция Согласован(Документ) Экспорт
		
	Согласован = Ложь;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ВЫБОР
		|		КОГДА СписокСогласованныхДокументов.Согласован = ЗНАЧЕНИЕ(Перечисление.СтатусСогласованияДокумента.Согласован)
		|			ТОГДА ИСТИНА
		|		ИНАЧЕ ЛОЖЬ
		|	КОНЕЦ КАК Согласован
		|ИЗ
		|	РегистрСведений.СписокСогласованныхДокументов КАК СписокСогласованныхДокументов
		|ГДЕ
		|	СписокСогласованныхДокументов.Документ = &Документ";
	
	Запрос.УстановитьПараметр("Документ", Документ);	
	Результат = Запрос.Выполнить().Выбрать();
	
	Пока Результат.Следующий() Цикл
		Согласован = Результат.Согласован;
	КонецЦикла;
	
	Возврат Согласован;
	
	
КонецФункции

#КонецОбласти